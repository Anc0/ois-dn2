function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = censorship(sporocilo);
    sporocilo = sporocilo.replaceAll("<","&lt");
    sporocilo = sporocilo.replaceAll(">","&gt");
    sporocilo = sporocilo.replaceAll(":)","<img src = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
    sporocilo = sporocilo.replaceAll(";)","<img src = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
    sporocilo = sporocilo.replaceAll("(y)","<img src = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
    sporocilo = sporocilo.replaceAll(":*","<img src = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
    sporocilo = sporocilo.replaceAll(":(","<img src = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

//funkcija, ki zamenja vse ponovitve niza original z nizom newString
String.prototype.replaceAll = function(original,newString) {
  return this.split(original).join(newString);
}

var socket = io.connect();
var sirina;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
      mojeUpIme = rezultat.vzdevek; //OO
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#kanali').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#kanali').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#kanali div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  $(window).resize(function(){
    $('#vsebina').css("width","100%");
  });

  //funkcije naloge 2.5==================================================================================
  
  socket.on('dodajSeznam', function(uporabnikiNaKanalu) {
    $('#uporabniki').empty();
    $('#uporabniki').append(uporabnikiNaKanalu.ime);
  }); 
  
  //=====================================================================================================
  //funkcije naloge 2.6========================================================================
  
  socket.on('napakaUporabnikNeObstajaZasebno', function(uporabnik) {
    var besedilo = "Sporocila "+uporabnik.sporocilo+
                    " uporabniku z vzdevkom "+
                    uporabnik.ime  +" ni bilo mogoce posredovati.";
    var novElement = $('<div style="font-weight: bold"></div>').text(besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('posredujZasebnoSporocilo', function(uporabnik) {
    if(mojeUpIme == uporabnik.ime) {
      var besedilo = uporabnik.posiljatelj + 
                      " (zasebno): " + 
                      uporabnik.sporocilo;
     var novElement = $('<div style="font-weight: bold"></div>').text(besedilo);
     $('#sporocila').append(novElement);
    }
  });
  
  var mojeUpIme = "";
  
  //===========================================================================================

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

//funkcija za cenzuro kletvic
function censorship(sporocilo) {
  var http = new XMLHttpRequest();
  http.open("GET","swearWords.xml",false);
  http.send();
  var doc = http.responseXML;
  var prepovedaneBesede = doc.getElementsByTagName("word");
  var trenutnaBeseda = "";
  var i,j,k;
  for(i = 0; i < prepovedaneBesede.length; i++) {
    trenutnaBeseda = prepovedaneBesede[i].childNodes[0].nodeValue;
    var cenzura = "";
    for(j = 0; j < trenutnaBeseda.length; j++) {
      cenzura += "*";
    }
    var dolzinabesede = j;
    var znak = "", besedaZaPrimerjavo = "", zacetek = 0;
    for(j = 0; j < sporocilo.length; j++) {
      znak = sporocilo.charAt(j);
      if(znak != " " && znak != "." && znak != "," && znak != "!" && znak != "?" ) besedaZaPrimerjavo += znak;
      if(znak == " " || znak == "." || znak == "," || znak == "!" || znak == "?" || j == (sporocilo.length - 1)) {
        if(besedaZaPrimerjavo.length == trenutnaBeseda.length) {
          var besedaZaPrimerjavoMale = besedaZaPrimerjavo.toLowerCase();
          for(k = 0; k < besedaZaPrimerjavo.length; k++) {
            if(besedaZaPrimerjavoMale.charAt(k) != trenutnaBeseda.charAt(k)) break;
          }
          if(k == dolzinabesede) {
            if(sporocilo == besedaZaPrimerjavo) sporocilo = sporocilo.replace(besedaZaPrimerjavo,cenzura);
            else if(zacetek === 0) sporocilo = sporocilo.replace(besedaZaPrimerjavo+" ",cenzura+" ");
            else if ((zacetek + dolzinabesede) == (sporocilo.length - 1)) sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo," "+cenzura);
            else sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo+" "," "+cenzura+" ");
            
            if(zacetek === 0) sporocilo = sporocilo.replace(besedaZaPrimerjavo+".",cenzura+".");
            else sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo+"."," "+cenzura+".");
            if(zacetek === 0) sporocilo = sporocilo.replace(besedaZaPrimerjavo+",",cenzura+",");
            else sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo+","," "+cenzura+",");
            if(zacetek === 0) sporocilo = sporocilo.replace(besedaZaPrimerjavo+"!",cenzura+"!");
            else sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo+"!"," "+cenzura+"!");
            if(zacetek === 0) sporocilo = sporocilo.replace(besedaZaPrimerjavo+"?",cenzura+"?");
            else sporocilo = sporocilo.replace(" "+besedaZaPrimerjavo+"?"," "+cenzura+"?");
          }
        }
        besedaZaPrimerjavo = "";
        zacetek = j;
      }
    }
  }
  return sporocilo;
}
  
  
