var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: "lksdjf6278945bg$%&J5g6..g456UGF)%mjau"
  });
};

Klepet.prototype.spremeniKodiranKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var rezultat = ukaz;
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  switch(ukaz) {
    case 'pridruzitev':
      var seznamBesed = rezultat.split('"');
      if(seznamBesed.length < 5) {
        besede.shift();
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal);
      }
      else {
        var izbraniKanal = seznamBesed[1], geslo = seznamBesed[3];
        this.spremeniKodiranKanal(izbraniKanal, geslo);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      var nick = "", besedilo = "";
      var seznamBesed = rezultat.split('"');
      if(seznamBesed.length < 5) {
        sporocilo = "Neznan ukaz.";
        break;
      }
      nick = seznamBesed[1];
      besedilo = seznamBesed[3];
      this.socket.emit('posljiZasebnoSporocilo', {
        ime: nick,
        sporocilo: besedilo
      });
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};


