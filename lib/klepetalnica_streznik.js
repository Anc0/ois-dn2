var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var nick = "";                      //globalna spremenljivka za up. ime uporabnika

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', true);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPosredovanjeZasebnegaSporocila(socket);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  nick = vzdevek;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, novUporabnik) {
  if(!novUporabnik) {
    izpisiUporabnikeNaKanaluVSeznamLokalnoString(trenutniKanal[socket.id], socket);
    izpisiUporabnikeNaKanaluVSeznamGlobalnoString(trenutniKanal[socket.id], socket);
  }
  socket.join(kanal);
  pushKanal("Skedenj","lksdjf6278945bg$%&J5g6..g456UGF)%mjau");
  trenutniKanal[socket.id] = kanal;
  var posljiKanal = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: trenutniKanal[socket.id], vzdevek: nick});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {           
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  izpisiUporabnikeNaKanaluVSeznamLokalnoString(kanal, socket);
  izpisiUporabnikeNaKanaluVSeznamGlobalnoString(kanal, socket);
  
}



function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') === 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } 
    else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        nick = vzdevek;
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: nick,
          kanal: trenutniKanal[socket.id]
        });
        izpisiUporabnikeNaKanaluVSeznamLokalnoString(trenutniKanal[socket.id], socket);
        izpisiUporabnikeNaKanaluVSeznamGlobalnoString(trenutniKanal[socket.id], socket);
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var zeObstaja = pushKanal(kanal.novKanal, kanal.geslo);
    var zascitenoZGeslom = true;
    for(var i in kanali) {
      if(kanali[i] == kanal.novKanal) {
        if(gesla[i] == "lksdjf6278945bg$%&J5g6..g456UGF)%mjau") {
          zascitenoZGeslom = false;
        }
      }
    }
    if(zeObstaja) {
      if(zascitenoZGeslom) {
        if(kanal.geslo == getPassword(kanal.novKanal)) {
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal.novKanal);
        }
        else {
          socket.emit('sporocilo', {
            besedilo:  "Pridruzitev v kanal " + kanal.novKanal + 
            " ni bilo uspesno, ker je geslo napačno!"
          });
        }
      }
      else {
        if(kanal.geslo == "lksdjf6278945bg$%&J5g6..g456UGF)%mjau") {
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal.novKanal);
        }
        else {
          socket.emit('sporocilo', {
            besedilo:  "Izbrani kanal " + kanal.novKanal + 
            " je prosto dostopen in ne zahteva prijave z"+
            " geslom, zato se prijavite z uporabo /pridruzitev" +
            " <kanal> ali zahtevajte kreiranje kanala z drugim imenom."
          });
        }
      }
    }
    else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);      
    }
    var seznamUporabnikovNaKanalu;
    for(var i in kanali) {
      seznamUporabnikovNaKanalu = io.sockets.clients(kanali[i]);
      if(seznamUporabnikovNaKanalu.length === 0) {
        kanali.splice(i,1);
        gesla.splice(i,1);
      }
    }
  });
}

// funkcije naloge 2.7 ==============================================================

function getPassword(kanal) {
  for(var i in kanali) {
    if(kanal == kanali[i]) return gesla[i];
  }
}

function pushKanal(kanal, geslo) {
  var zeObstaja = false;
  for(var i in kanali) {
    if(kanali[i] == kanal) {
      zeObstaja = true;
      break;
    }
  }
  if(!zeObstaja) {
    kanali.push(kanal);
    gesla.push(geslo);
  }
  return zeObstaja;
}

var kanali = [];
var gesla = [];
//===================================================================================


function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
//funkcije naloge 2.5==================================================================================

function izpisiUporabnikeNaKanaluVSeznamLokalnoString(kanal, socket) {
  var seznamUporabnikovKanala = "";
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    seznamUporabnikovKanala += vzdevkiGledeNaSocket[uporabnikSocketId] + "<br>";
  }
  socket.emit('dodajSeznam', {
    ime: seznamUporabnikovKanala
  });
}

function izpisiUporabnikeNaKanaluVSeznamGlobalnoString(kanal, socket) {
  var seznamUporabnikovKanala = "";
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    seznamUporabnikovKanala += vzdevkiGledeNaSocket[uporabnikSocketId] + "<br>";
  }
  socket.broadcast.to(kanal).emit('dodajSeznam', {
    ime: seznamUporabnikovKanala
  });
}

//=====================================================================================================
//funkcije naloge 2.6 ========================================================================
  
function obdelajPosredovanjeZasebnegaSporocila(socket) {
  socket.on('posljiZasebnoSporocilo', function(uporabnik) {
    var vzdevek = uporabnik.ime, sporocilo = uporabnik.sporocilo;
    if (uporabljeniVzdevki.indexOf(vzdevek) == -1 || vzdevek == vzdevkiGledeNaSocket[socket.id]) {
      socket.emit('napakaUporabnikNeObstajaZasebno',{
        ime: vzdevek,
        sporocilo: sporocilo
      });
    }
    else {
      socket.broadcast.emit('posredujZasebnoSporocilo', {
        ime: vzdevek,
        sporocilo: sporocilo,
        posiljatelj: vzdevkiGledeNaSocket[socket.id]
      });
    }
  });
}

//============================================================================================
